use embed_manifest::{embed_manifest, new_manifest, manifest};

fn main() {
    if std::env::var_os("CARGO_CFG_WINDOWS").is_some() {
        let manifest =new_manifest("Lesha.Inc.Matrix").dpi_awareness(manifest::DpiAwareness::System);

        embed_manifest(manifest)
            .expect("unable to embed manifest file");
    }
    println!("cargo:rerun-if-changed=build.rs");
}