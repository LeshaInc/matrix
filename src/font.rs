use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;

use anyhow::{anyhow, bail, Context, Result};

pub struct Font {
    pub atlas: Atlas,
    pub layout: Layout,
}

impl Font {
    pub fn load(atlas: impl AsRef<Path>, layout: impl AsRef<Path>) -> Result<Font> {
        Ok(Font {
            atlas: Atlas::from_png(atlas)?,
            layout: Layout::from_csv(layout)?,
        })
    }
}

pub struct Atlas {
    pub data: Vec<u8>,
    pub width: usize,
    pub height: usize,
}

impl Atlas {
    pub fn from_png(path: impl AsRef<Path>) -> Result<Atlas> {
        let path = path.as_ref();
        let file = File::open(path).with_context(|| format!("Cannot open {}", path.display()))?;
        let decoder = png::Decoder::new(BufReader::new(file));
        let (info, mut reader) = decoder.read_info().unwrap();

        if info.color_type != png::ColorType::RGBA || info.bit_depth != png::BitDepth::Eight {
            bail!("Bad font atlas");
        }

        let width = info.width as usize;
        let height = info.height as usize;

        let mut buf = vec![0; 4 * width * height];

        for y in 0..height {
            let out = &mut buf[4 * width * y..4 * width * (y + 1)];
            let row = &reader.next_row()?.unwrap();
            out.copy_from_slice(&row[..out.len()]);
        }

        Ok(Atlas {
            data: buf,
            width,
            height,
        })
    }
}

pub struct Layout {
    pub glyphs: HashMap<char, GlyphLayout>,
}

impl Layout {
    pub fn from_csv(path: impl AsRef<Path>) -> Result<Layout> {
        let mut glyphs = HashMap::new();

        let path = path.as_ref();
        let file = File::open(path).with_context(|| format!("Cannot open {}", path.display()))?;
        let reader = BufReader::new(file);

        for line in reader.lines() {
            let line = line.context("Read error")?;
            let mut split = line.split(',');

            let char = split
                .next()
                .and_then(|v| v.parse::<u32>().ok())
                .and_then(|v| std::char::from_u32(v))
                .ok_or_else(|| anyhow!("Invalid font CSV"))?;

            let mut floats = [0.0; 9];
            for (i, v) in split.take(9).enumerate() {
                floats[i] = v.parse::<f32>().map_err(|_| anyhow!("Invalid font CSV"))?;
            }

            let layout = GlyphLayout {
                advance: floats[0],
                baseline_bounds: Rect::new(floats[1], floats[2], floats[3], floats[4]),
                atlas_bounds: Rect::new(floats[5], floats[6], floats[7], floats[8]),
            };

            glyphs.insert(char, layout);
        }

        Ok(Layout { glyphs })
    }
}

#[derive(Clone, Copy)]
pub struct Rect {
    pub x0: f32,
    pub y0: f32,
    pub x1: f32,
    pub y1: f32,
}

impl Rect {
    pub fn new(x0: f32, y0: f32, x1: f32, y1: f32) -> Self {
        Self { x0, y0, x1, y1 }
    }
}

#[derive(Clone, Copy)]
pub struct GlyphLayout {
    pub advance: f32,
    pub baseline_bounds: Rect,
    pub atlas_bounds: Rect,
}
