use std::time::Instant;

use anyhow::Result;
use raw_window_handle::{HasRawWindowHandle, RawWindowHandle};
use winit::dpi::PhysicalSize;
use winit::event::{Event, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::{Window, WindowBuilder};

use super::{App, Backend, BackendBuilder, BackendKind};

pub struct WinitBackend {
    event_loop: Option<EventLoop<()>>,
    window: Window,
    size: PhysicalSize<u32>,
    last_update: Instant,
    last_render: Instant,
}

pub struct WinitBackendBuilder;

impl BackendBuilder for WinitBackendBuilder {
    fn kind(&self) -> BackendKind {
        BackendKind::Window
    }

    fn name(&self) -> &'static str {
        "winit"
    }

    fn build(&self) -> Result<Box<dyn Backend>> {
        let event_loop = EventLoop::new();
        let window = WindowBuilder::new()
            .with_title("Matrix")
            .build(&event_loop)?;

        let size = window.inner_size();

        let last_update = Instant::now();
        let last_render = Instant::now();

        Ok(Box::new(WinitBackend {
            event_loop: Some(event_loop),
            window,
            size,
            last_update,
            last_render,
        }))
    }
}

impl WinitBackend {
    fn handle_event(
        &mut self,
        event: Event<()>,
        control_flow: &mut ControlFlow,
        app: &mut dyn App,
    ) -> Result<()> {
        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                *control_flow = ControlFlow::Exit;
            }

            Event::WindowEvent {
                event: WindowEvent::Resized(new_size),
                ..
            } => {
                if self.size != new_size {
                    self.size = new_size;
                    app.resize([self.size.width, self.size.height])?;
                }
            }

            Event::MainEventsCleared => {
                loop {
                    let update_dt = app.update_dt();
                    if self.last_update.elapsed() < update_dt {
                        break;
                    }

                    app.update()?;
                    self.last_update += update_dt;
                }

                if self.last_render.elapsed() >= app.render_dt() {
                    app.render()?;
                    self.last_render = Instant::now();
                }

                *control_flow = ControlFlow::WaitUntil(std::cmp::min(
                    self.last_update + app.update_dt(),
                    self.last_render + app.render_dt(),
                ));
            }

            _ => {}
        }

        Ok(())
    }
}

impl Backend for WinitBackend {
    fn get_window(&mut self) -> RawWindowHandle {
        self.window.raw_window_handle()
    }

    fn get_resolution(&mut self) -> [u32; 2] {
        let size = self.window.inner_size();
        [size.width, size.height]
    }

    fn run_loop(mut self: Box<Self>, mut app: Box<dyn App>) {
        let lp = self.event_loop.take().unwrap();
        lp.run(move |event, _, control_flow| {
            if let Err(e) = self.handle_event(event, control_flow, &mut *app) {
                eprintln!("Error: {:?}", e);
                *control_flow = ControlFlow::Exit;
            }
        })
    }
}
