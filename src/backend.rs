#[cfg(target_os = "windows")]
mod windows;
mod winit;

use std::time::Duration;

use anyhow::Result;
use raw_window_handle::RawWindowHandle;

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum BackendKind {
    Wallpaper,
    Window,
}

pub trait BackendBuilder {
    fn kind(&self) -> BackendKind;

    fn name(&self) -> &'static str;

    fn build(&self) -> Result<Box<dyn Backend>>;
}

pub trait Backend {
    fn get_window(&mut self) -> RawWindowHandle;

    fn get_resolution(&mut self) -> [u32; 2];

    fn run_loop(self: Box<Self>, app: Box<dyn App>);
}

pub trait App {
    fn update_dt(&self) -> Duration;

    fn update(&mut self) -> Result<()>;

    fn render_dt(&self) -> Duration;

    fn render(&mut self) -> Result<()>;

    fn resize(&mut self, resolution: [u32; 2]) -> Result<()>;
}

pub fn enumerate_backends() -> Vec<Box<dyn BackendBuilder>> {
    let mut backends: Vec<Box<dyn BackendBuilder>> = Vec::new();

    backends.push(Box::new(self::winit::WinitBackendBuilder));

    #[cfg(target_os = "windows")]
    backends.push(Box::new(self::windows::WindowsBackendBuilder));

    backends
}
