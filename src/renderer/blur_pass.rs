use std::path::Path;

use anyhow::Result;
use wgpu::{
    BindGroupLayout, BlendComponent, BlendFactor, BlendOperation, BlendState, ColorTargetState,
    ColorWrites, CommandEncoder, Device, FragmentState, LoadOp, Operations,
    PipelineLayoutDescriptor, PushConstantRange, RenderPassColorAttachment, RenderPipeline,
    RenderPipelineDescriptor, Sampler, ShaderStages, TextureFormat, TextureView, VertexState,
};

use super::{create_bind_group, create_shader, view_as_bytes};

pub struct BlurPass {
    pipeline: RenderPipeline,
}

impl BlurPass {
    pub fn new(
        device: &Device,
        bind_group_layout: &BindGroupLayout,
        format: TextureFormat,
        res_path: &Path,
    ) -> Result<BlurPass> {
        let pipeline = create_pipeline(device, bind_group_layout, format, res_path)?;
        Ok(BlurPass { pipeline })
    }

    pub fn run(
        &self,
        device: &Device,
        bind_group_layout: &BindGroupLayout,
        sampler: &Sampler,
        encoder: &mut CommandEncoder,
        src: &TextureView,
        dst: &TextureView,
        dir: [f32; 2],
        clear: Option<wgpu::Color>,
    ) {
        let bind_group = create_bind_group(device, bind_group_layout, sampler, src);

        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &[RenderPassColorAttachment {
                view: &dst,
                resolve_target: None,
                ops: Operations {
                    load: clear.map(LoadOp::Clear).unwrap_or(LoadOp::Load),
                    store: true,
                },
            }],
            depth_stencil_attachment: None,
        });

        rpass.set_pipeline(&self.pipeline);
        rpass.set_bind_group(0, &bind_group, &[]);
        rpass.set_push_constants(ShaderStages::FRAGMENT, 0, view_as_bytes(&dir));
        rpass.draw(0..3, 0..1);
    }
}

fn create_pipeline(
    device: &Device,
    bind_group_layout: &BindGroupLayout,
    format: TextureFormat,
    res_path: &Path,
) -> Result<RenderPipeline> {
    let vert_shader = create_shader(device, res_path.join("shaders/blur.vert.spv"))?;
    let frag_shader = create_shader(device, res_path.join("shaders/blur.frag.spv"))?;

    let layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
        label: None,
        bind_group_layouts: &[bind_group_layout],
        push_constant_ranges: &[PushConstantRange {
            stages: ShaderStages::FRAGMENT,
            range: 0..16,
        }],
    });

    Ok(device.create_render_pipeline(&RenderPipelineDescriptor {
        label: None,
        layout: Some(&layout),
        vertex: VertexState {
            module: &vert_shader,
            entry_point: "main",
            buffers: &[],
        },
        primitive: Default::default(),
        depth_stencil: None,
        multisample: Default::default(),
        fragment: Some(FragmentState {
            module: &frag_shader,
            entry_point: "main",
            targets: &[ColorTargetState {
                format,
                blend: Some(BlendState {
                    alpha: BlendComponent {
                        src_factor: BlendFactor::One,
                        dst_factor: BlendFactor::OneMinusSrcAlpha,
                        operation: BlendOperation::Add,
                    },
                    color: BlendComponent {
                        src_factor: BlendFactor::SrcAlpha,
                        dst_factor: BlendFactor::OneMinusSrcAlpha,
                        operation: BlendOperation::Add,
                    },
                }),
                write_mask: ColorWrites::all(),
            }],
        }),
    }))
}
