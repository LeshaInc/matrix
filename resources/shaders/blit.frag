#version 450

layout(location = 0) in vec2 vTex;

layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0) uniform sampler uSampler;
layout(set = 0, binding = 1) uniform texture2D uTexture;

void main() {
    outColor = texture(sampler2D(uTexture, uSampler), vTex);
}
